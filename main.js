var http = require('http');
var express = require('express');
var url = require('url');
var logicaNegocioFactory = require('./logicanegocio');


var app = express();


app.get('/vuelos', function (request, response) {
    var database;
    var logicaNegocio = logicaNegocioFactory(database);

    var parametros = url.parse(request.url, true);

    var query = parametros.query;

    var origen = query.origen;
    var destino = query.destino;

    var vuelosList = logicaNegocio.consultarVuelos(origen, destino);
    response.send(vuelosList);
});

http.createServer(app).listen('9000', function () {
    console.log('Microservicio Escuchando en el puerto 9000');
});
