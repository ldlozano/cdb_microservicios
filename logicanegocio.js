var ProgramacionLogica = function (database) {
    return {
        consultarVuelos: function (origen, destino) {
            var vuelos = database.getVuelo(origen, destino);
            return vuelos;
        },
        adicionarVuelo: function (vuelo) {
            return database.insert(vuelo);
        },
        cancelarVuelo: function (idVuelo) {
            return database.delete(idVuelo);
        }
    }
}

module.exports = ProgramacionLogica;
